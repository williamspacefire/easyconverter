package tk.spacefire.easyconverter

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

import kotlinx.android.synthetic.main.activity_converter.*
import kotlinx.android.synthetic.main.content_converter.*
import org.jetbrains.anko.*
import tk.spacefire.easyconverter.helper.Ads
import tk.spacefire.easyconverter.service.ConvertService
import java.io.File

class ConverterActivity : AppCompatActivity() {

    private var type: Int = 0
    private var format: Int = 0
    private var exportPathManuallyChanged: Boolean = false
    private var backPressed = false

    companion object {
        const val EXTERNAL_STORAGE_PERMISSION = 1
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_converter)
        setSupportActionBar(toolbar)

        extenssion.text = getExt()

        input_file.setText(Environment.getExternalStorageDirectory().absolutePath)

        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        bitrateText.text = "${resources.getString(R.string.convert_bitrate_label)} ${bitrate.progress}kbps"

        if (intent.extras?.getInt("type") != null) {
            type = intent.extras!!.getInt("type")
        }

        if (intent.extras?.getInt("format") != null) {
            format = intent.extras!!.getInt("format")
        }

        //Checking Storage Permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            //Check if we need to explain why we need to access the Storage
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                alert(
                        title = resources.getString(R.string.permission_storage_title),
                        message = resources.getString(R.string.permission_storage_message)
                ) {
                    yesButton {
                        ActivityCompat.requestPermissions(
                                this@ConverterActivity,
                                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                EXTERNAL_STORAGE_PERMISSION
                        )
                    }
                }.show()
            } else {
                ActivityCompat.requestPermissions(
                        this@ConverterActivity,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        EXTERNAL_STORAGE_PERMISSION
                )
            }

        }

        finder_btn.setOnClickListener {
            startActivityForResult(
                    Intent(this, SelectorActivity::class.java).putExtra("fun", 0),
                    1
            )
        }

        export_path_btn.setOnClickListener {
            startActivityForResult(
                    Intent(this, SelectorActivity::class.java).putExtra("fun", 1),
                    2
            )
        }

        btn_format.setOnClickListener {
            selector(resources.getString(R.string.conversion_convert_to), convertTypes) {_ , t ->
                selector(resources.getString(R.string.conversion_select_format),
                        formats[t].toList()) {_, f ->
                    type = t
                    format = f

                    extenssion.text = getExt()
                }
            }
        }

        fab.setOnClickListener { _ ->

            if (!input_file.text.toString().isEmpty() &&
                    !new_path.text.toString().isEmpty() &&
                    !new_name.text.toString().isEmpty()
            ) {
                val i = Intent(ctx, ConvertService::class.java)
                        .putExtra("input", input_file.text.toString())
                        .putExtra("bitrate", "${bitrate.progress}k")
                        .putExtra("output_folder", new_path.text.toString())
                        .putExtra("output_filename",
                                "${new_name.text.toString()}${extenssion.text.toString()}")

                startService(i)
            }
        }

        bitrate.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                if (p1 < 45) bitrate.progress = 45
                bitrateText.text = "${resources.getString(R.string.convert_bitrate_label)} ${bitrate.progress}kbps"
            }

        })
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {

            val file = File(data.extras!!.getString("file"))
            val newName = file.name.replace("."+file.extension, "")

            input_file.setText(file.absolutePath)
            new_name.setText(newName)

            if (!exportPathManuallyChanged) new_path.setText(file.absolutePath.replace(file.name, ""))
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK && data != null) {
            new_path.setText(data.extras!!.getString("folder")+"/")
            exportPathManuallyChanged = true
        }

    }

    fun getExt(): String = if (type == 0) audioInfo[format][0]
        else if (type == 1) videoInfo[format][0]
        else imageInfo[format][0]

    /*override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> finish()
        }

        return super.onOptionsItemSelected(item)
    }*/

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
