package tk.spacefire.easyconverter

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.app_bar_main.*
import nl.bravobit.ffmpeg.FFmpeg
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity() {

    private var ffmpegIsSupported: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val ffmpeg = FFmpeg.getInstance(ctx)
        ffmpegIsSupported = ffmpeg.isSupported

        if (!ffmpegIsSupported) ffmpegIsNotSupportedDialog() else {
            startActivity<ConverterActivity>("type" to 0, "format" to 0)
            finish()
        }

        fab.setOnClickListener { _ ->
            startJob()
        }
    }

    private fun ffmpegIsNotSupportedDialog() {
        alert(
                title = resources.getString(R.string.ffmpeg_not_supported_title),
                message = resources.getString(R.string.ffmpeg_not_supported_message)
        ) {
            isCancelable = false
            yesButton {
                finish()
            }
        }.show()
    }

    private fun startJob() {
        selector("Convert to", convertTypes) {_ , i ->
            selector("Convert to", formats[i].toList()) {_, f ->
                startActivity<ConverterActivity>("type" to i, "format" to f)
            }
        }
    }
}
