package tk.spacefire.easyconverter

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_selector.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.ctx
import org.jetbrains.anko.yesButton
import tk.spacefire.easyconverter.adapter.SelectorRecyclerViewAdapter
import tk.spacefire.easyconverter.interfaces.OnClickListener
import java.io.File

class SelectorActivity : AppCompatActivity(), OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val sdcard = Environment.getExternalStorageDirectory()
    private var currentDirectory = sdcard
    private var function: Int = 0

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selector)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        function = intent.extras!!.getInt("fun")

        if (function == 0 )
            supportActionBar!!.title = "Choose a file"
        else
            supportActionBar!!.title = "Choose a folder"

        select_folder.setOnClickListener {
            setResult(
                    Activity.RESULT_OK,
                    Intent().putExtra("folder", currentDirectory!!.absolutePath)
            )
            finish()
        }

        if (function == 0) select_folder.visibility = View.INVISIBLE

        //Checking Storage Permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            //Check if we need to explain why we need to access the Storage
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                alert(
                        title = resources.getString(R.string.permission_storage_title),
                        message = resources.getString(R.string.permission_storage_message)
                ) {
                    yesButton {
                        ActivityCompat.requestPermissions(
                                this@SelectorActivity,
                                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                ConverterActivity.EXTERNAL_STORAGE_PERMISSION
                        )
                    }
                }.show()
            } else {
                ActivityCompat.requestPermissions(
                        this@SelectorActivity,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        ConverterActivity.EXTERNAL_STORAGE_PERMISSION
                )
            }

        } else {

            viewManager = LinearLayoutManager(this)
            viewAdapter = SelectorRecyclerViewAdapter(currentDirectory!!, this)

            recyclerView = recycler_files.apply {
                layoutManager = viewManager
                adapter = viewAdapter
            }
        }
    }

    override fun onClick(position: Int) {
        if (currentDirectory!!.listFiles()[position].isFile && function == 0) {
            setResult(
                    Activity.RESULT_OK,
                    Intent().putExtra("file", currentDirectory!!.listFiles()[position].absolutePath)
            )
            finish()
        } else if (currentDirectory!!.listFiles()[position].isDirectory)
            currentDirectory = File(currentDirectory!!.listFiles()[position].absolutePath)
            viewAdapter = SelectorRecyclerViewAdapter(currentDirectory!!, this)
            recyclerView.adapter = viewAdapter
            viewAdapter.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        currentDirectory = File(
                currentDirectory!!.absolutePath.replace("/"+currentDirectory!!.name, "")
        )

        if (currentDirectory!!.listFiles() == null) return super.onBackPressed()

        viewAdapter = SelectorRecyclerViewAdapter(currentDirectory!!, this)
        recyclerView.adapter = viewAdapter
        viewAdapter.notifyDataSetChanged()
    }

    /*override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item!!.itemId) {
            android.R.id.home -> finish()
        }

        return super.onOptionsItemSelected(item)
    }*/

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>, grantResults: IntArray) {

        when (requestCode) {
            ConverterActivity.EXTERNAL_STORAGE_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewManager = LinearLayoutManager(this)
                    viewAdapter = SelectorRecyclerViewAdapter(currentDirectory!!, this)

                    recyclerView = recycler_files.apply {
                        layoutManager = viewManager
                        adapter = viewAdapter
                    }
                } else {
                    alert(
                            title = resources.getString(R.string.permission_storage_title),
                            message = resources.getString(R.string.permission_storage_denied_message)
                    ) {
                        yesButton {
                            finish()
                        }
                    }.show()
                }

                return
            }
        }

    }
}
