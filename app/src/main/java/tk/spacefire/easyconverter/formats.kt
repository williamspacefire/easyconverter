package tk.spacefire.easyconverter

val convertTypes = listOf("AUDIO","VIDEO"/*, "IMAGE"*/)

val formats = arrayOf(

        // Audio formats
        arrayOf(
                "MP3",
                "AAC",
                "FLAC",
                "OPUS",
                "WAV",
                "AIFF",
                "OGG",
                "WMA",
                "M4A",
                "AC3",
                "AU"
        ),

        // Video formats
        arrayOf(
                "MP4",
                "AVI",
                "MKV",
                "OGG",
                "WMV",
                "WEBM",
                "FLV",
                "MOV",
                "VOB"
        )/*,
        arrayOf(
                "GIF",
                "PNG",
                "JPG",
                "JPEG",
                "BMP"
        )*/
)

// Most follow the order above
val audioInfo = arrayOf(
        arrayOf(".mp3"),
        arrayOf(".aac",".m4a"),
        arrayOf(".flac"),
        arrayOf(".opus"),
        arrayOf(".wav"),
        arrayOf(".aiff"),
        arrayOf(".ogg"),
        arrayOf(".wma"),
        arrayOf(".m4a"),
        arrayOf(".ac3"),
        arrayOf(".au")
)

val videoInfo = arrayOf(
        arrayOf(".mp4"),
        arrayOf(".avi"),
        arrayOf(".mkv"),
        arrayOf(".ogg"),
        arrayOf(".wmv"),
        arrayOf(".webm"),
        arrayOf(".flv"),
        arrayOf(".mov"),
        arrayOf(".vob")
)

val imageInfo = arrayOf(
        arrayOf(".gif"),
        arrayOf(".png"),
        arrayOf(".jpg"),
        arrayOf(".jpgeg"),
        arrayOf(".bmp")
)