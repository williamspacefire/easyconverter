package tk.spacefire.easyconverter.service

import android.app.NotificationManager
import android.app.Service
//import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.os.IBinder
import androidx.core.app.NotificationCompat
import nl.bravobit.ffmpeg.FFcommandExecuteResponseHandler
import nl.bravobit.ffmpeg.FFmpeg
import nl.bravobit.ffmpeg.FFtask
import org.jetbrains.anko.ctx
import org.jetbrains.anko.toast
import tk.spacefire.easyconverter.R
//import tk.spacefire.easyconverter.database.Database
import java.io.File

class ConvertService : Service() {

    private lateinit var notificationManager : NotificationManager
    private lateinit var ffmpeg: FFmpeg
    private val ffmpegTask: MutableList<FFtask> = mutableListOf()
    private val taskId: MutableList<Int> = mutableListOf()
    private var putInForeground = true
    //private lateinit var db: Database

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        ffmpeg = FFmpeg.getInstance(ctx)
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        //db = Room.databaseBuilder(applicationContext, Database::class.java, "mediadb").build()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        taskId.add(startId)

        val input = intent.extras!!.getString("input")
        var outputFolder = intent.extras!!.getString("output_folder")
        val outputFilename = intent.extras!!.getString("output_filename")
        val bitrate = intent.extras!!.getString("bitrate")
        val NOTIFICATION_ID = startId
        val outputFile = File(outputFolder+outputFilename)
        var duration = 0

        if (outputFile.exists()) outputFile.delete()
        if (!outputFolder!!.last().equals("/")) outputFolder += "/"

        val convertNotification = NotificationCompat.Builder(ctx)
                .setSmallIcon(R.drawable.ic_converter)
                .setWhen(0)
                .setOngoing(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentTitle(outputFilename)

        val handler = object : FFcommandExecuteResponseHandler {

            override fun onStart() {

            }

            override fun onFinish() {
                notificationManager.cancel(NOTIFICATION_ID)
                taskId.remove(startId)

                if (taskId.size == 0) {
                    stopSelf()
                } else {
                    putInForeground = true
                }
            }

            override fun onSuccess(message: String?) {
                notificationManager.notify(NOTIFICATION_ID+5,
                        NotificationCompat.Builder(ctx)
                                .setSmallIcon(R.drawable.ic_action_done)
                                .setWhen(System.currentTimeMillis())
                                .setContentTitle(outputFilename)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                                .setContentText("$outputFilename ${resources.getString(R.string.conversion_sucessfully)}")
                                .build())
            }

            override fun onFailure(message: String?) {
                notificationManager.notify(NOTIFICATION_ID+9,
                        NotificationCompat.Builder(ctx)
                                .setSmallIcon(R.drawable.ic_converter_error)
                                .setWhen(System.currentTimeMillis())
                                .setContentTitle(outputFilename)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                                .setContentText(resources.getString(R.string.conversion_failed))
                                .build())
            }

            override fun onProgress(message: String) {
                if (putInForeground) {
                    startForeground(NOTIFICATION_ID, convertNotification.build())
                    putInForeground = false
                }

                val inputDuration = "Duration: [0-9]{2}:[0-9]{2}:[0-9]{2}(.[0-9]{2})?".toRegex()
                val outputProgress = "time=[0-9]{2}:[0-9]{2}:[0-9]{2}(.[0-9]{2})?".toRegex()

                val d = matchResult(inputDuration, message)
                        ?.replace("Duration: ", "")
                        ?.replace(":", "")
                        ?.replace(".", "")
                        ?.toInt()

                if (d != null) {
                    duration = d+1
                    convertNotification.setProgress(duration, 0, false)
                    notificationManager.notify(NOTIFICATION_ID, convertNotification.build())
                }

                val progress = matchResult(outputProgress, message)
                        ?.replace("time=","")
                        ?.replace(":", "")
                        ?.replace(".", "")
                        ?.toInt()

                if (progress != null) {
                    val percentage = "%.2f".format((progress.toFloat() / duration) * 100)
                    convertNotification
                            .setContentText("$percentage%")
                            .setProgress(duration, progress, false)
                }

                notificationManager.notify(NOTIFICATION_ID, convertNotification.build())
            }
        }

        ffmpegTask.add(ffmpegTask.size, ffmpeg.execute(
                arrayOf(
                        "-i",
                        input,
                        "-ab",
                        bitrate,
                        outputFolder+outputFilename
                ),
                handler
        ))

        return START_REDELIVER_INTENT
    }

    fun matchResult(regex: Regex, input: CharSequence): String? {
        if (regex.containsMatchIn(input)) {
            val result = regex.find(input)

            return result?.value
        } else return null
    }

    override fun onDestroy() {
        var unfinishedTasks = 0

        for (task in ffmpegTask) {
            if (!task.isProcessCompleted && task.killRunningProcess()) unfinishedTasks++
        }

        if (unfinishedTasks > 0) toast("${unfinishedTasks} ${resources.getString(R.string.conversion_stopped)}")
    }
}
