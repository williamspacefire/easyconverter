package tk.spacefire.easyconverter.interfaces

interface OnClickListener {
    fun onClick(position: Int)
}