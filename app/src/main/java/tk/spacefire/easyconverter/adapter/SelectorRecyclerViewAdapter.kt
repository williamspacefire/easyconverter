package tk.spacefire.easyconverter.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tk.spacefire.easyconverter.interfaces.OnClickListener
import tk.spacefire.easyconverter.R
import java.io.File

class SelectorRecyclerViewAdapter(
        private val files: File,
        private val clickListener: OnClickListener
): RecyclerView.Adapter<SelectorRecyclerViewAdapter.ViewHolder>() {

    class ViewHolder(
            val view: View,
            val textView: TextView = view.findViewById<TextView>(R.id.selector_file_name),
            val imageView: ImageView = view.findViewById(R.id.selector_file_type_icon)
    ): RecyclerView.ViewHolder(view)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): SelectorRecyclerViewAdapter.ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.selector_list_file, parent, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textView.text = files.listFiles()[position].name
        if(files.listFiles()[position].isDirectory)
            holder.imageView.setImageResource(R.drawable.ic_file_folder)
        else
            holder.imageView.setImageResource(R.drawable.ic_file_file)

        holder.view.setOnClickListener {
            clickListener.onClick(position)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = if (files.listFiles() != null) files.listFiles().size else 0

}